<div id="fh5co-header">
    <header id="fh5co-header-section">
        <div class="container">
            <div class="nav-header">
                <a href="#" class="js-fh5co-nav-toggle fh5co-nav-toggle"><i></i></a>
                <h1 id="fh5co-logo"><a href="/">Inte<span>lygenz</span></a></h1>
                <!-- START #fh5co-menu-wrap -->
                <nav id="fh5co-menu-wrap" role="navigation">
                    <ul class="sf-menu" id="fh5co-primary-menu">
                            <#list nav_items as nav_item>

                                            <#assign
                                                nav_item_attr_has_popup = ""
                                                nav_item_attr_selected = ""
                                                nav_item_css_class = ""
                                                nav_item_layout = nav_item.getLayout()
                                            />

                                            <#if nav_item.isSelected()>
                                                <#assign
                                                nav_item_attr_has_popup = "aria-haspopup='true'"
                                                nav_item_attr_selected = "aria-selected='true'"
                                                nav_item_css_class = "active"
                                                />
                                            </#if>
                                <li class="${nav_item_css_class}">
                                    <a href="${nav_item.getURL()}" ${nav_item.getTarget()}>${nav_item.getName()}</a>
                                </li>
                            </#list>
                    </ul>
                </nav>
            </div>
        </div>
    </header>
</div>

<div class="fh5co-hero">
    <div class="fh5co-overlay"></div>
    <div class="fh5co-cover text-center" data-stellar-background-ratio="0.5"
         style="background-image: url(${images_folder}/home-image.jpg);">
        <div class="desc animate-box">
            <h2>Guardian Free HTML5 Template</h2>
            <span>Lovely Crafted by <a href="http://frehtml5.co/" target="_blank" class="fh5co-site-name">FREEHTML5.co</a></span>
            <span><a class="btn btn-primary" href="#">Get Started</a></span>
        </div>
    </div>

</div>