<!DOCTYPE html>

<#include init />

<html class="${root_css_class}" dir="<@liferay.language key="lang.dir" />" lang="${w3c_language_id}">

<head>
    <title>${the_title} - ${company_name}</title>

    <meta content="initial-scale=1.0, width=device-width" name="viewport"/>

	<@liferay_util["include"] page=top_head_include />

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300' rel='stylesheet' type='text/css'>

    <!-- Animate.css -->
    <link rel="stylesheet" href="${css_folder}/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="${css_folder}/icomoon.css">
    <!-- Superfish -->
    <link rel="stylesheet" href="${css_folder}/superfish.css">

    <link rel="stylesheet" href="${css_folder}/style.css">


    <!-- Modernizr JS -->
    <script src="${javascript_folder}/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="${javascript_folder}/respond.min.js"></script>
    <![endif]-->

</head>

<body class="${css_class}">

<@liferay_ui["quick-access"] contentId="#main-content" />

<@liferay_util["include"] page=body_top_include />

<@liferay.control_menu />

<#--<div class="container-fluid" id="wrapper">-->

        <#if is_signed_in>
            <div class="container-fluid" id="wrapper">
            <header id="banner" role="banner">
                <div id="heading">

                </div>

            <#if has_navigation && is_setup_complete>
                <#include "${full_templates_path}/navigation_signed.ftl" />
            </#if>
            </header>
        <#else>
        <div id="fh5co-wrapper">
            <div id="fh5co-page">
            <#include "${full_templates_path}/navigation_unsigned.ftl" />
        </#if>


        <section id="content">
            <h1 class="hide-accessible">${the_title}</h1>

        <#--<nav id="breadcrumbs">
            <@liferay.breadcrumbs />
        </nav>-->

		<#if selectable>
            <@liferay_util["include"] page=content_include />
        <#else>
            ${portletDisplay.recycle()}

            ${portletDisplay.setTitle(the_title)}

            <@liferay_theme["wrap-portlet"] page="portlet.ftl">
                <@liferay_util["include"] page=content_include />
            </@>
        </#if>
        </section>

        <footer id="footer" role="contentinfo">
            <p class="powered-by">
			<@liferay.language key="powered-by" /> <a href="http://www.liferay.com" rel="external">Liferay</a>
            </p>
        </footer>
    </div>
<#if !is_signed_in></div></#if>


<@liferay_util["include"] page=body_bottom_include />

<@liferay_util["include"] page=bottom_include />

    <!-- inject:js -->
        <!-- jQuery Easing -->
        <script src="${javascript_folder}/jquery.easing.1.3.js"></script>
        <!-- Bootstrap -->
        <script src="${javascript_folder}/bootstrap.min.js"></script>
        <!-- Waypoints -->
        <script src="${javascript_folder}/jquery.waypoints.min.js"></script>
        <!-- Stellar -->
        <script src="${javascript_folder}/jquery.stellar.min.js"></script>
        <!-- Superfish -->
        <script src="${javascript_folder}/hoverIntent.js"></script>
        <script src="${javascript_folder}/superfish.js"></script>

        <!-- Main JS (Do not remove) -->
        <script src="${javascript_folder}/templateMain.js"></script>

    <!-- endinject -->



</body>

</html>