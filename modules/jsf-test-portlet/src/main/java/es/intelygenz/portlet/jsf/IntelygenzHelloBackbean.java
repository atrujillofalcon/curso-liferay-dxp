package es.intelygenz.portlet.jsf;

import com.liferay.faces.util.context.FacesContextHelperUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.ActionEvent;

@RequestScoped
@ManagedBean
public class IntelygenzHelloBackbean {

    public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void submit(ActionEvent actionEvent){
        FacesContextHelperUtil.addGlobalSuccessInfoMessage();
    }
}
