package es.intelygenz.listener.user;

import com.liferay.portal.kernel.exception.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.BaseModelListener;
import com.liferay.portal.kernel.model.ModelListener;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ThemeLocalServiceUtil;
import com.liferay.portal.kernel.util.StringPool;
import org.osgi.service.component.annotations.Component;

/**
 * Los ModelListener se usan para realizar alguna acción antes o después de crear, modificar o borrar una entidad
 * del portal
 *
 * @author Arnaldo Trujillo
 */
@Component(
        immediate = true,
        service = ModelListener.class
)
public class UserModelListener extends BaseModelListener<User> {

    private static final String IGZ_MAIL_DOMAIN = "intelygenz.com";

    private static Log LOG = LogFactoryUtil.getLog(UserModelListener.class);

    @Override
    public void onBeforeCreate(User model) throws ModelListenerException {
        normalizeEmployInformation(model);
    }

    @Override
    public void onBeforeUpdate(User model) throws ModelListenerException {
        normalizeEmployInformation(model);
    }

    private void normalizeEmployInformation(User model) {
        //calculamos el id de empleado (Primera letra del nombre + apellidos) ex: atrujillofalcon
        String idEmpleado = (StringPool.BLANK + model.getFirstName().charAt(0) + model.getLastName())
                .toLowerCase().replace(StringPool.SPACE, StringPool.BLANK);

        //verificamos si existe el campo personalizado "id_empleado" y en el caso afirmativo guardamos el valor
        if (model.getExpandoBridge().hasAttribute("id_empleado")) {
            model.getExpandoBridge().setAttribute("id_empleado", idEmpleado);
        }
        model.setEmailAddress(idEmpleado + "@" + IGZ_MAIL_DOMAIN);
    }
}