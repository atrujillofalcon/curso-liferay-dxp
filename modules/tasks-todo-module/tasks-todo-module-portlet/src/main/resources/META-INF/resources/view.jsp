<%@ page import="javax.portlet.PortletURL" %>
<%@ include file="init.jsp" %>

<portlet:actionURL name="/create/new/task" var="createTaskURL"/>

<%
    PortletURL iterUrl = renderResponse.createRenderURL();
%>


<aui:form action="${createTaskURL}" method="post">
    <aui:input name="title" type="text" placeholder="Titulo"/>
    <aui:input name="description" type="textarea" placeholder="Descripcion"/>
    <button type="submit">Crear nuevo</button>
</aui:form>

<br>

<liferay-ui:search-container total="${total}" delta="5" emptyResultsMessage="no-tasks">
    <liferay-ui:search-container-results results="${allTask}"/>
    <liferay-ui:search-container-row className="es.intelygenz.portlet.servicebuilder.model.Task" modelVar="curTask" escapedModel="true">
        <liferay-ui:search-container-column-text property="title"/>
        <liferay-ui:search-container-column-text property="description"/>
        <liferay-ui:search-container-column-date property="createDate"/>
        <liferay-ui:search-container-column-text>
            <portlet:actionURL name="/delete/task" var="deleteTaskURL">
                <portlet:param name="taskId" value="<%=String.valueOf(curTask.getTaskId())%>"/>
            </portlet:actionURL>
            <a class="btn btn-danger" href="${deleteTaskURL}">Borrar</a>
        </liferay-ui:search-container-column-text>
    </liferay-ui:search-container-row>
    <liferay-ui:search-iterator searchContainer="<%=searchContainer%>"/>
</liferay-ui:search-container>