package es.intelygenz.portlet.servicebuilder.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import es.intelygenz.portlet.servicebuilder.service.TaskLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=taskintelygenzportlet",
                "mvc.command.name=/delete/task"
        },
        service = MVCActionCommand.class
)
public class DeleteTaskCommand extends BaseMVCActionCommand {

    private static final Log LOG = LogFactoryUtil.getLog(DeleteTaskCommand.class);

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        Long taskId = ParamUtil.getLong(actionRequest,"taskId");
        TaskLocalServiceUtil.deleteTask(taskId);
    }
}
