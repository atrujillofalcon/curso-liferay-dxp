package es.intelygenz.portlet.servicebuilder.portlet.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import es.intelygenz.portlet.servicebuilder.service.TaskLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

/**
 * @author Arnaldo Trujillo
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.intelygenz",
                "com.liferay.portlet.instanceable=true",
                "com.liferay.portlet.requires-namespaced-parameters=false",
                "javax.portlet.name=taskintelygenzportlet",
                "javax.portlet.display-name=Task Intelygenz-Portlet",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.init-param.view-template=/view.jsp",
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user"
        },
        service = Portlet.class
)
public class TasksTodoModulePortlet extends MVCPortlet {

    @Override
    public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        super.doEdit(renderRequest, renderResponse);
    }

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        renderRequest.setAttribute("allTask", TaskLocalServiceUtil.getTasks(-1, -1));
        renderRequest.setAttribute("total", TaskLocalServiceUtil.getTasksCount());
        super.doView(renderRequest, renderResponse);
    }
}