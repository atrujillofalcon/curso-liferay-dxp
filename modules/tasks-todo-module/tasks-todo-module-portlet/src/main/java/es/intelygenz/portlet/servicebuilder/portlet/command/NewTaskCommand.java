package es.intelygenz.portlet.servicebuilder.portlet.command;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import es.intelygenz.portlet.servicebuilder.model.Task;
import es.intelygenz.portlet.servicebuilder.service.TaskLocalServiceUtil;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import java.time.Instant;
import java.util.Date;

@Component(
        immediate = true,
        property = {
                "javax.portlet.name=taskintelygenzportlet",
                "mvc.command.name=/create/new/task"
        },
        service = MVCActionCommand.class
)
public class NewTaskCommand extends BaseMVCActionCommand {

    private static final Log LOG = LogFactoryUtil.getLog(NewTaskCommand.class);

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);

        //obtenemos un nuevo id para esta entidad
        //CounterLocalServiceUtil autoincrementa el id
        long taskId = CounterLocalServiceUtil.increment(Task.class.getName());

        //obtenemos los datos que vienen del formulario
        String title = ParamUtil.get(actionRequest, "title", "Default Title");
        String description = ParamUtil.get(actionRequest, "description", "Default Description");
        Boolean isFavorite = ParamUtil.get(actionRequest, "description", false);

        //creamos la nueva entidad
        Task task = TaskLocalServiceUtil.createTask(taskId);

        //seteamos todos los metacampos en la entidad
        task.setCreateDate(Date.from(Instant.now()));
        task.setModifiedDate(Date.from(Instant.now()));
        task.setUserId(themeDisplay.getUserId());
        task.setGroupId(themeDisplay.getScopeGroupId());
        task.setCompanyId(themeDisplay.getCompanyId());

        //seteamos todos los campos específicos de la entidad
        task.setTitle(title);
        task.setDescription(description);
        task.setIsFavorite(isFavorite);

        //persistimos la entidad en base de datos
        task.persist();

        //añadimos un mensaje de éxito
        SessionMessages.add(actionRequest, "add.msg.sucess");
    }
}
