/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package es.intelygenz.portlet.servicebuilder.service.impl;

import aQute.bnd.annotation.ProviderType;
import es.intelygenz.portlet.servicebuilder.model.Task;
import es.intelygenz.portlet.servicebuilder.service.base.TaskLocalServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the task local service.
 * <p>
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link es.intelygenz.portlet.servicebuilder.service.TaskLocalService} interface.
 * <p>
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TaskLocalServiceBaseImpl
 * @see es.intelygenz.portlet.servicebuilder.service.TaskLocalServiceUtil
 */
@ProviderType
public class TaskLocalServiceImpl extends TaskLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. Always use {@link es.intelygenz.portlet.servicebuilder.service.TaskLocalServiceUtil} to access the task local service.
     */
    public List<Task> getTaskOrderedByTitle(String title) {
        return taskPersistence.findByTitle(title);
    }

}