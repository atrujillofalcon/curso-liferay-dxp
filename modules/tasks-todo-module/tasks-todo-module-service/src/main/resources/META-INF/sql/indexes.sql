create index IX_B35237EB on Intelygenz_Task (title[$COLUMN_LENGTH:75$]);
create index IX_3EB7F131 on Intelygenz_Task (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_533D0FF3 on Intelygenz_Task (uuid_[$COLUMN_LENGTH:75$], groupId);