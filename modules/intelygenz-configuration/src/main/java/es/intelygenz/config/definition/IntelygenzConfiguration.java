package es.intelygenz.config.definition;

import aQute.bnd.annotation.metatype.Meta;
import com.liferay.portal.configuration.metatype.annotations.ExtendedObjectClassDefinition;
import es.intelygenz.config.util.PortletKeys;

/**
 * @author arnaldo
 */
@ExtendedObjectClassDefinition(category = "Intelygenz", scope = ExtendedObjectClassDefinition.Scope.PORTLET_INSTANCE)
@Meta.OCD(
        localization = "content/Language",
        name = "IntelygenzConfiguration.configuration.name",
        id = PortletKeys.IGZ_CONFIG_PID
)
public interface IntelygenzConfiguration {

    @Meta.AD(deflt = "100", required = false)
    public int maxTaskAmount();

    @Meta.AD(deflt = "true", required = false)
    public boolean includeFacebookButton();

}