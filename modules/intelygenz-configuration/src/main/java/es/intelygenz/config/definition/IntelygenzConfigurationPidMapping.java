package es.intelygenz.config.definition;

import com.liferay.portal.kernel.settings.definition.ConfigurationPidMapping;
import es.intelygenz.config.util.PortletKeys;
import org.osgi.service.component.annotations.Component;

/**
 * @author Arnaldo Trujillo
 */
@Component
public class IntelygenzConfigurationPidMapping implements ConfigurationPidMapping {

    /**
     * getConfigurationBeanClass: Returns the configuration bean class.
     *
     * @return Class The configuration bean class.
     */
    @Override
    public Class<?> getConfigurationBeanClass() {
        return IntelygenzConfiguration.class;
    }

    /**
     * getConfigurationPid: Returns the portlet id for the config.
     *
     * @return String The portlet id.
     */
    @Override
    public String getConfigurationPid() {
        return PortletKeys.IGZ_CONFIG_PID;
    }

}