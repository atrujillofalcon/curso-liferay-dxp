package es.intelygenz.config.util;

/**
 * @author Arnaldo Trujillo
 */
public class PortletKeys {

    public static final String IGZ_CONFIG_PID = "es.intelygenz.config.definition.IntelygenzConfiguration";

}
