package es.intelygenz.portlet.mvc.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import es.intelygenz.portlet.mvc.portlet.util.Constant;
import org.osgi.service.component.annotations.Component;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

/**
 * Comando para renderizar un jsp específico. Hace la función de controlador.
 * Importante específicar javax.portlet.name el mismo valor que tenga el portlet.
 * La propiedad mvc.command.name es el enlace con el <liferay-portlet:renderUrl> creado en el fichero jsp
 *
 * @author Arnaldo Trujillo
 */
@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + Constant.PORTLET_NAME,
                "mvc.command.name=/intelygenz/greetings"
        }, service = MVCRenderCommand.class
)
public class HelloCommand implements MVCRenderCommand {

    private static final Log LOG = LogFactoryUtil.getLog(HelloCommand.class);

    @Override
    public String render(RenderRequest renderRequest, RenderResponse renderResponse) throws PortletException {
        LOG.info("Entering in greetings method");

        String savedGreetings = renderRequest.getPreferences().getValue("greetingsPrefix", "Hola");
        renderRequest.setAttribute("greetingsPrefix", savedGreetings);

        String name = ParamUtil.getString(renderRequest, "name");
        renderRequest.setAttribute("nombre", name);

        return "/greetings.jsp";
    }

}
