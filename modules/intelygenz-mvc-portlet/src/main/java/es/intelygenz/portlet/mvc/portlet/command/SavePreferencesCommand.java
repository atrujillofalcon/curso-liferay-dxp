package es.intelygenz.portlet.mvc.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;
import es.intelygenz.portlet.mvc.portlet.util.Constant;
import org.osgi.service.component.annotations.Component;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletPreferences;

/**
 * Comando guardar un valor en las preferencias del portlet. Hace la función de controlador.
 * Importante específicar javax.portlet.name el mismo valor que tenga el portlet.
 * La propiedad mvc.command.name es el enlace con el <liferay-portlet:actionURL> creado en el fichero jsp
 *
 * @author Arnaldo Trujillo
 */
@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + Constant.PORTLET_NAME,
                "mvc.command.name=/intelygenz/preferences/save"
        },
        service = MVCActionCommand.class
)
public class SavePreferencesCommand extends BaseMVCActionCommand {

    private static final Log LOG = LogFactoryUtil.getLog(SavePreferencesCommand.class);

    @Override
    protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {
        LOG.info("Entering in greetings action");
        PortletPreferences preferences = actionRequest.getPreferences();

        //guardamos un valor en las preferencias del portlet
        String greetingsPrefixValue = ParamUtil.get(actionRequest, "greetingsPrefix", "Hola");
        preferences.setValue("greetingsPrefix", greetingsPrefixValue);
        preferences.store();

        LOG.info("Stored preferences success");
    }
}
