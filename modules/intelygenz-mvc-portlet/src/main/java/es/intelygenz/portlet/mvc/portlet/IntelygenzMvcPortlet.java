package es.intelygenz.portlet.mvc.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import es.intelygenz.portlet.mvc.portlet.util.Constant;
import org.osgi.service.component.annotations.Component;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;

/**
 * Componente que define la configuración del portlet. Ver todas las configuraciones disponibles en
 * @see <a href="https://dev.liferay.com/es/develop/reference/-/knowledge_base/7-0/portlet-descriptor-to-osgi-service-property-map">PORTLET DESCRIPTOR</a>
 *
 * Importante especificar requires-namespaced-parameters a false para evitar problemas a la hora de pasar los parámetros
 * del controlador a la vista
 * javax.portlet.portlet-mode=text/html;edit Habilita el modo edición de las preferencias del portlet
 *
 * @author Arnaldo Trujillo
 */
@Component(
        immediate = true,
        property = {
                "com.liferay.portlet.display-category=category.intelygenz",
                "com.liferay.portlet.instanceable=true",
                "com.liferay.portlet.requires-namespaced-parameters=false",
                "javax.portlet.name=" + Constant.PORTLET_NAME,
                "javax.portlet.display-name=Intelygenz Mvc Portlet",
                "javax.portlet.init-param.template-path=/",
                "javax.portlet.resource-bundle=content.Language",
                "javax.portlet.security-role-ref=power-user,user",
                "javax.portlet.portlet-mode=text/html;edit",
                "javax.portlet.init-param.view-template=/mvcView.jsp",
                "javax.portlet.init-param.edit-template=/mvcEdit.jsp",
        },
        service = Portlet.class
)
public class IntelygenzMvcPortlet extends MVCPortlet {

    @Override
    public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        super.doView(renderRequest, renderResponse);
    }

    @Override
    public void doEdit(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
        super.doEdit(renderRequest, renderResponse);
    }
}