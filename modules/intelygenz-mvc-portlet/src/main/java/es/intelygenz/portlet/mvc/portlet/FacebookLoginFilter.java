package es.intelygenz.portlet.mvc.portlet;

import es.intelygenz.portlet.mvc.portlet.util.Constant;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;
import javax.portlet.filter.RenderFilter;
import javax.servlet.ServletContext;
import java.io.IOException;

/**
 * Filtro que se ejecutará antes de procesar cada petición del portlet.
 * Es importante especificar javax.portlet.name para definir a que portlet se le aplicará el filtro
 *
 * @author Arnaldo Trujillo
 */
@Component(
        immediate = true,
        property = {
                "javax.portlet.name=" + Constant.PORTLET_NAME
        },
        service = PortletFilter.class
)
public class FacebookLoginFilter implements RenderFilter {

    private ServletContext _servletContext;

    @Override
    public void doFilter(RenderRequest request, RenderResponse response, FilterChain chain) throws IOException, PortletException {

        // lo metemos en la request para que esté disponible en el jsp
        request.setAttribute("facebookLoginServletContext", _servletContext);
        chain.doFilter(request, response);
    }

    @Override
    public void init(FilterConfig filterConfig) throws PortletException {
        //no hacemos nada aquí
    }

    @Override
    public void destroy() {
        //no hacemos nada aquí
    }

    //Inyectamos el ServiceContext del loginFacebook para después pasarlo en la request
    @Reference(target = "(osgi.web.symbolicname=com.liferay.login.authentication.facebook.connect.web)", unbind = "-")
    protected void setServletContext(ServletContext servletContext) {
        _servletContext = servletContext;
    }


}