<%@ include file="init.jsp" %>

<portlet:actionURL name="/intelygenz/preferences/save" var="savePreferencesActionURL"/>

<aui:form action="${savePreferencesActionURL}" method="post">
    <label for="greetingsPrefix"><liferay-ui:message key="intelygenz-mvc-portlet.hello.greeting-prefix"/></label>
    <input id="greetingsPrefix" type="text" name="greetingsPrefix"/>
    <button type="submit">Guardar</button>
</aui:form>