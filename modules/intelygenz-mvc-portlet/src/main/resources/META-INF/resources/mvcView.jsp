<%@ include file="init.jsp" %>

<portlet:renderURL var="greetingsRenderURL">
    <portlet:param name="mvcRenderCommandName" value="/intelygenz/greetings"/>
</portlet:renderURL>

<aui:form action="${greetingsRenderURL}" method="post">
    <label for="nameInput"><liferay-ui:message key="intelygenz-mvc-portlet.intro"/></label>
    <input id="nameInput" type="text" name="name"/>
    <button type="submit">Saludar</button>
</aui:form>

<%--Aqui insertamos el facebook.jsp, el truco está en obtener primero el ServletContext de lo que se quiera insertar y pasarlo en la request. Mirar LoginFilter--%>
<liferay-util:include page="/html/portlet/login/navigation/facebook.jsp" servletContext="<%=((ServletContext)request.getAttribute("facebookLoginServletContext"))%>"/>