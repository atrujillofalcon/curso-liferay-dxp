#**Curso de Liferay DXP**

Workspace con módulos didácticos de cada funcionalidad impartida en el temario del curso.
  Los módulos que se incluyen son los siguientes:  
    1. intelygenz-mvc-portlet (Portlet MVC con comandos ACTION y RENDER)  
    2. tasks-todo-module (Portlet, API y capa de Servicio construidos con Service Builder y que simulan y guardan en base de datos una aplicación de tareas pendientes)  
    3. jsf-test-portlet (Portlet JSF con un Backbean de prueba)  
    
  Se creará un tema de apariencia llamado intelygenz-theme en base a una plantilla de Bootstrap 3  
  ##Principales comandos para desarrollar temas:  
  Generar tema: `yo liferay-theme`  
  Compilar tema: `gulp build`  
  Desplegar tema: `gulp deploy`  
  
  
  
##Principales comandos blade

Para descargar e inicializar el servidor:
  `blade gw initBundle`   
  Para arrancar y detener el servidor(modo debug):
  `blade server start -d` y `blade server stop`  
  Para compilar:
  `blade gw build`  
  Para desplegar al bundle local:
  `blade gw deploy`
  